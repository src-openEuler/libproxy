Name:           libproxy
Version:        0.5.3
Release:        3
Summary:        Libproxy is a library that provides automatic proxy configuration management

License:        LGPL-2.1-or-later
URL:            https://libproxy.github.io/libproxy/
Source0:        https://github.com/libproxy/%{name}/archive/refs/tags/%{version}.tar.gz#/%{name}-%{version}.tar.gz

BuildRequires:  gcc meson vala gi-docgen chrpath
BuildRequires:  pkgconfig(gio-2.0) >= 2.71.3 pkgconfig(gsettings-desktop-schemas) python3-devel
BuildRequires:  pkgconfig(duktape) pkgconfig(gobject-introspection-1.0) pkgconfig(libcurl)

Provides:       %{name}-bin %{name}-gnome %{name}-kde %{name}-networkmanager %{name}-pacrunner 
Obsoletes:      %{name}-bin %{name}-gnome %{name}-kde %{name}-mozjs %{name}-networkmanager %{name}-pacrunner 
Provides:       %{name}-duktape = %{version}-%{release} python3-%{name} = %{version}-%{release}
Obsoletes:      %{name}-duktape < %{version}-%{release} python3-%{name} < %{version}-%{release}

%description
libproxy offers the following features:
    * extremely small core footprint (< 35K).
    * no external dependencies within libproxy core.
      (libproxy plugins may have dependencies).
    * only 3 functions in the stable external API.
    * dynamic adjustment to changing network topology.
    * a standard way of dealing with proxy settings across all scenarios.

%package        devel
Summary:        Libproxy provides consistent proxy configuration to applications - Development Files
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description    devel
This package contains libraries and header files for developing applications.

%package_help

%prep
%autosetup -p1

%build
%meson -Dconfig-gnome=true -Dconfig-kde=true -Dconfig-osx=false -Dconfig-windows=false -Dintrospection=true -Dtests=true -Dvapi=true
%meson_build

%install
%meson_install
chrpath -d $RPM_BUILD_ROOT%{_libdir}/*.so*
chrpath -d $RPM_BUILD_ROOT%{_libdir}/%{name}/*.so
mkdir -p $RPM_BUILD_ROOT/etc/ld.so.conf.d
echo "%{_libdir}/libproxy" > $RPM_BUILD_ROOT/etc/ld.so.conf.d/%{name}-%{_arch}.conf

%check
%meson_test

%ldconfig_scriptlets

%files
%defattr(-,root,root)
%doc CHANGELOG.md README.md
%license COPYING
%config(noreplace) /etc/ld.so.conf.d/*
%{_libdir}/*.so.*
%{_libdir}/libproxy/libpxbackend-1.0.so
%{_libdir}/girepository-1.0/
%{_bindir}/proxy

%files devel
%defattr(-,root,root)
%{_includedir}/libproxy/
%{_libdir}/libproxy.so
%{_libdir}/pkgconfig/libproxy-1.0.pc
%{_datadir}/vala/vapi/
%{_datadir}/gir-1.0

%files help
%defattr(-,root,root)
%{_mandir}/man8/proxy.8*
%{_datadir}/doc/libproxy-1.0/

%changelog
* Mon Jul 01 2024 zhouyihang <zhouyihang3@h-partners.com> - 0.5.3-3
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:fix so file not found in non-64-bit env

* Tue Aug 29 2023 zhouyihang <zhouyihang3@h-partners.com> - 0.5.3-2
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:remove rpath

* Fri Aug 04 2023 zhouyihang <zhouyihang3@h-partners.com> - 0.5.3-1
- Type:requirements
- Id:NA
- SUG:NA
- DESC:update libproxy to 0.5.3

* Tue Nov 08 2022 zhouyihang <zhouyihang3@h-partners.com> - 0.4.18-1
- Type:requirements
- Id:NA
- SUG:NA
- DESC:update libproxy to 0.4.18

* Sat Apr 16 2022 yanglu <yanglu72@h-partners.com> - 0.4.17-2
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:fix libproxy compile error based on python3.10

* Thu Jan 28 2021 xihaochen <xihaochen@huawei.com> - 0.4.17-1
- Type:requirements
- Id:NA
- SUG:NA
- DESC:update libproxy to 0.4.17

* Wed Dec 02 2020 xihaochen <xihaochen@huawei.com> - 0.4.15-19
- Type:requirements
- Id:NA
- SUG:NA
- DESC:delete mozjs-68 dependency

* Fri Oct 30 2020 xihaochen <xihaochen@huawei.com> - 0.4.15-18
- Type:requirements
- Id:NA
- SUG:NA
- DESC:delete python2 dependency

* Thu Oct 29 2020 liuxin <liuxin264@huawei.com> - 0.4.15-17
- Type:cves
- Id:NA
- SUG:NA
- DESC:Disable mozjs backend by default and fix CVE-2020-25219

* Tue Oct 27 2020 orange-snn <songnannan2@huawei.com> - 0.4.15-16
- change the mozjs52 to mozjs68 in buildRequires

* Tue Oct 20 2020 hanzhijun <hanzhijun1@huawei.com> - 0.4.15-15
- Type:cves
- Id:NA
- SUG:NA
- DESC:fix CVE-2020-26154

* Thu Aug 6 2020 zengwefeng <zwfeng@huawei.com> - 0.4.15-14
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:drop dependency of libmodman


* Sat Jan 11 2020 openEuler Buildteam <buildteam@openeuler.org> - 0.4.15-13
- Type:NA
- ID:NA
- SUG:NA
- DESC:fix patch source

* Thu Nov 28 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.4.15-12
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:remove the webkitgtk4 package from main package

* Thu Oct 17 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.4.15-11
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:remove the /usr/bin/kreadconfig5 in buildRequires.

* Tue Sep 10 2019 liyongqiang<liyongqiang10@huawei.com> - 0.4.15-10
- Package init
